import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../components/home/Home.vue'

Vue.use(VueRouter)

const routes = [
    {
        path :'*',
        name : 'home',
        component : Home
    }
]

const router = new VueRouter({
    mode: 'history',
  base: __dirname,
  routes
})

export default router