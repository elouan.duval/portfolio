import Vue from 'vue'
import VueAgile from 'vue-agile'
import router from './router/index.ts'
import App from './App'


Vue.use(VueAgile)

Vue.config.productionTip = false

new Vue({
   render: h => h(App),
   router
})
 .$mount('#app') // si retiré la vue n'est pas affiché
